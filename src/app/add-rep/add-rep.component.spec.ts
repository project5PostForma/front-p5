import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRepComponent } from './add-rep.component';

describe('AddRepComponent', () => {
  let component: AddRepComponent;
  let fixture: ComponentFixture<AddRepComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddRepComponent]
    });
    fixture = TestBed.createComponent(AddRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
