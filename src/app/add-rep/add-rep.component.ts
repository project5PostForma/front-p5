import { Component, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Rep } from '../models/rep.model';
import { User } from '../models/user.model';
import { SharedService } from '../shared.service';
import { UserService } from '../user.service';


@Injectable({
  providedIn: 'root'
})


@Component({
  selector: 'app-add-rep',
  templateUrl: './add-rep.component.html',
  styleUrls: ['./add-rep.component.scss']
})
export class AddRepComponent implements OnInit{

  rep: Rep = new Rep();
  selectedUser!: User;

  isRepAdded: boolean = false;
  user: User = new User(); // Declare the user property here

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private sharedService: SharedService
  ){}

  ngOnInit(): void {
    this.rep.title = ""
    this.rep.beginDate = new Date()
    this.rep.compositor = ""
  
    this.route.params.subscribe(params => {
      console.log("in addrep")
      const id = params['id'];
      this.getUser(id);
    });

    this.selectedUser = this.sharedService.getSelectedUser();


    // this.route.queryParams.subscribe(params => {
    //   this.cliff = params['cliff'];
    // });
  }

  getUser(userId: number) {
    this.userService.getUser(userId).subscribe({
      next: (data: any) => {
        this.user = data;
                // Set the selectedUser in the shared service
                this.sharedService.setSelectedUser(this.user);
      },
      error: (error: any) => {
        console.log("Error on getting a single user: " + error);
      }
    });
  }

  
  addRep(rep: Rep){
    console.log("add rep")
    this.isRepAdded = !this.isRepAdded
    this.userService.addRepToUser(this.user.id, rep).subscribe({
      next: (data: any) => {
        // this.reps.seeRep(user)
        //this.onRepAdded()
        //this.router.navigate(['/rep'], { queryParams: { userId: this.user.id } });
        this.router.navigateByUrl(`/rep/${this.user.id}`);
      },
      error: (error: any) => {
        console.log("error on adding rep" + error);
      }
    })
    
    }

    // After adding a rep


onRepAdded() {
  const userId = this.user.id; // Get the user ID from the current user
  // Perform rep addition logic, then navigate back to the "rep" page with the user ID as a query parameter
  this.router.navigate(['/rep'], { queryParams: { userId } });
}

}
