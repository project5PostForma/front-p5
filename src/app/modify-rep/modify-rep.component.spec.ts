import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyRepComponent } from './modify-rep.component';

describe('ModifyRepComponent', () => {
  let component: ModifyRepComponent;
  let fixture: ComponentFixture<ModifyRepComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ModifyRepComponent]
    });
    fixture = TestBed.createComponent(ModifyRepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
