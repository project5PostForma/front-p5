import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Rep } from '../models/rep.model';
import { User } from '../models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-modify-rep',
  templateUrl: './modify-rep.component.html',
  styleUrls: ['./modify-rep.component.scss']
})
export class ModifyRepComponent {
  rep: Rep = new Rep();
  user: User = new User();
  isModifiedRep = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    // get the id from list component
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.getRep(id);
    });
  }

  getRep(repId: number) {
    this.userService.getUserSingleRep(repId).subscribe({
      next: (data: any) => {
        this.rep = data;
      },
      error: (error: any) => {
        console.log("Error on getting a single rep: " + error);
      }
    });
  }


  modifyRep(id: number) {
    // this.isModifProfil = true
    console.log("rep id : " + id)
    const updatedRep: Rep = {
      id: id,
      title: this.rep.title, // Assign new name value
      compositor: this.rep.compositor, // Assign new firstname value
      beginDate: this.rep.beginDate,
      userId: this.rep.userId
    };
    this.userService
      .modifyRepById(id, updatedRep)
      .subscribe(() => {
        this.rep = updatedRep;
        this.router.navigateByUrl(`/rep/${this.rep.userId}`);
      });
    }
}
