import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/user.service';
import { Lesson } from '../models/lesson.model';
import { User } from '../models/user.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit{
  sortKey: string = '';
  sortDirection: string = 'asc';
  userList: User[] = []; // Define the userList property to hold a list of users

  constructor(
    private userService: UserService,
    private router: Router,
  ){}

  ngOnInit(): void {
    this.getAllUsers();
  }

  addUser(){
    this.router.navigate(['/addUser'] );
  }

  addLessonToUser(id: number){
    const lessonInstance = new Lesson();
    // Initialize the lesson instance with the provided ID or any other necessary data
    lessonInstance.date = new Date();
    lessonInstance.isPayed = true
    console.log(lessonInstance)

  // Assuming this.userService.addLessonToUser handles the addition of a lesson to a user
  this.userService.addLessonToUser(id, lessonInstance).subscribe(
    () => {
      console.log(`Lesson added to user with ID: ${id}`);
      // Optionally, you can refresh the user list or perform any other necessary actions
    },
    (error) => {
      console.error(`Error adding lesson to user with ID ${id}:`, error);
      // Handle any error that occurred during the addition of the lesson.
    }
  );
}

  deleteUserById(id: number) {
    console.log("del" + id);
    this.userService.deleteUserById(id).subscribe(
      () => {
        console.log(`Deleted user with ID: ${id}`);
        this.getAllUsers();
        // this.cdr.detectChanges(); // Trigger change detection
      },
      (error) => {
        console.error(`Error deleting user with ID ${id}:`, error);
        // Handle any error that occurred during the delete request.
      }
    );
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe({
      next: (data: any) => {
        this.userList = data;
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    })
  }

  redirectToModify(id: number){
    this.router.navigate(['/modifyUser', id]);
  }


  // sort(key: string) {
  //   if (this.sortKey === key) {
  //     this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
  //   } else {
  //     this.sortKey = key;
  //     this.sortDirection = 'asc';
  //   }

  //   this.userList.sort((a, b) => {
  //     let valueA = a[this.sortKey] as string;
  //     let valueB = b[this.sortKey] as string;

  //     if (valueA < valueB) {
  //       return this.sortDirection === 'asc' ? -1 : 1;
  //     } else if (valueA > valueB) {
  //       return this.sortDirection === 'asc' ? 1 : -1;
  //     } else {
  //       return 0;
  //     }
  //   });
  // }



}
