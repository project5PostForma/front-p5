import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-modify-user',
  templateUrl: './modify-user.component.html',
  styleUrls: ['./modify-user.component.scss']
})
export class ModifyUserComponent implements OnInit {
  userId!: number;
  isModifiedUser = false;
  isModifiedLesson = false;
  user: User = new User(); // Declare the user property here

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    // get the id from list component
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.getUser(id);
    });
  }

  getUser(userId: number) {
    this.userService.getUser(userId).subscribe({
      next: (data: any) => {
        this.user = data;
      },
      error: (error: any) => {
        console.log("Error on getting a single user: " + error);
      }
    });
  }

  selectDay(selectedDay: Event) {
    // Implement your logic when a day is selected
    console.log('Selected day:', selectedDay);
  }

  selectSlot(selectedSlot: Event) {
    // Implement your logic when a day is selected
    console.log('Selected day:', selectedSlot);
  }

  days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'];
  slots = ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']

  modifyLesson(){
    this.isModifiedLesson = !this.isModifiedLesson
    console.log("modif")
  }

  modifyUser(id: number) {
    // this.isModifProfil = true
    const updatedUser: User = {
      id: id,
      name: this.user.name, // Assign new name value
      firstname: this.user.firstname, // Assign new firstname value
      slot: this.user.slot,
      phone: this.user.phone,
      price: this.user.price,
      lessonDay: this.user.lessonDay,
      lessonDuration: this.user.lessonDuration,
      lessons: this.user.lessons,
      reps: this.user.reps,
      paymentStatus : this.user.paymentStatus,
      givenLessons : this.user.givenLessons
    };
    console.log(updatedUser)
    this.userService
      .modifyUserById(id, updatedUser)
      .subscribe(() => {
        this.user = updatedUser;
        console.log('User updated:', updatedUser);
        this.router.navigate(['/list']);

      });
    }
    
}
