import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js/auto';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit{

  ngOnInit() {
    const dates = ['2023-09-01', '2023-09-02', '2023-09-03']; // Add your dates
    const values = [10, 15, 8]; // Add your values

    // Create a canvas reference
    const canvas = document.getElementById('myChart') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');

    // Create the chart
    if (ctx){
    const myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: dates,
        datasets: [{
          label: 'My Data',
          data: values,
          borderColor: 'rgb(75, 192, 192)',
          borderWidth: 2,
        }],
      },
      options: {
        scales: {
          x: {
            type: 'time',
            time: {
              unit: 'day', // You can adjust the time unit as needed
            },
          },
          y: {
            beginAtZero: true,
          },
        },
      },
    });
  }
  }
}
