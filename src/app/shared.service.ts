

// shared.service.ts
import { Injectable } from '@angular/core';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private selectedUser!: User;

  setSelectedUser(user: User) {
    this.selectedUser = user;
  }

  getSelectedUser(): User {
    return this.selectedUser;
  }
}
