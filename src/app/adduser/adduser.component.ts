import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Lesson } from '../models/lesson.model';
import { User } from '../models/user.model';
import { UserService } from '../user.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.scss'],
  providers: [DatePipe]
})
export class AdduserComponent implements OnInit {

  isUserAdded = false
  user: User = new User();
  lessons!: Lesson[];
  formattedDate!: string | number | Date;

  constructor(
    private userService: UserService,
    private router: Router,
    private datePipe: DatePipe
  ){}


  ngOnInit(): void {
    this.user.lessonDay = "Lundi"
    this.user.slot = "09:00"
    this.user.lessonDuration = "1h"
    this.user.price = 40
    this.user.paymentStatus = 0
    this.user.givenLessons = 0
  }


  
  addUser(user: User){
    console.log("add user")
    this.isUserAdded = !this.isUserAdded
    this.initializeUserLessonsForYear(user);
    this.userService.addUser(user).subscribe({
      next: (data: any) => {
        this.router.navigate(['/list'] );
        console.log(user)
      },
      error: (error: any) => {
        console.log("error on adding user" + error);
        console.log(user)
      }
    })
    
    }

    private initializeUserLessonsForYear(user: User) {
      // Get the current year
      const currentYear = new Date().getFullYear();
    
      // Initialize an array to store lessons
      user.lessons = [];
    
      // Iterate through the year, checking if each date is on the desired day of the week
      for (let day = new Date(); day.getFullYear() === currentYear; day.setDate(day.getDate() + 1)) {
        const currentDay = new Date(day.getTime() - 24 * 60 * 60 * 1000);
        if (this.getDayNameInFrench(currentDay) === user.lessonDay) {
          
          // Create a new Lesson object and add it to the user's lessons
          const lesson = {
            date: new Date(currentDay.getTime() + 24 * 60 * 60 * 1000), // Use the formattedDate here
            day: user.lessonDay,
            isPayed: false, // Set the initial payment status
            userId: user.id,
          };
          user.lessons.push(lesson);

        

        }
      }
      
      console.log(user.lessons);
      return user.lessons;
    }
    
  


    getDayNameInFrench(date: Date): string {
      const days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
      return days[date.getDay()];
    }

    addYearLessonsToUser(id: number){
      console.log("addYearLessonsToUser")

    // Assuming this.userService.addLessonToUser handles the addition of a lesson to a user
    this.userService.addYearLessonsToUser(id, this.user.lessons).subscribe(
      () => {
        console.log(`Lesson added to user with ID: ${id}`);
        // Optionally, you can refresh the user list or perform any other necessary actions
      },
      (error) => {
        console.error(`Error adding lesson to user with ID ${id}:`, error);
        // Handle any error that occurred during the addition of the lesson.
      }
    );
  }

  selectDay(selectedDay: Event) {
    // Implement your logic when a day is selected
    console.log('Selected day:', selectedDay);
  }

  selectSlot(selectedSlot: Event) {
    // Implement your logic when a day is selected
    console.log('Selected day:', selectedSlot);
  }

  selectDuration(selectedDuration: Event) {
    // Implement your logic when a day is selected
    console.log('Selected duration:', selectedDuration);
  }

  duration = ['30 min', '45 min', '1h']
  days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
  slots = ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00']

}
