import { Component, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Rep } from '../models/rep.model';
import { User } from '../models/user.model';
import { SharedService } from '../shared.service';
import { UserService } from '../user.service';

@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-repertoires',
  templateUrl: './repertoires.component.html',
  styleUrls: ['./repertoires.component.scss']
})
export class RepertoiresComponent implements OnInit{

  repList!: Rep[]
  userList: User[] = []; // Define the userList property to hold a list of users
  isUserSelected: { [userId: string]: boolean } = {};
  user: User = new User();
  rep: Rep = new Rep();
  selectedUser!: User;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private sharedService: SharedService

  ){
    this.userList.forEach((user) => {
      this.isUserSelected[user.id] = false;
    });
  }


  ngOnInit(): void {
    console.log("init");
    this.getAllUsers();
  
    
  }

  getUser(userId: number) {
    this.userService.getUser(userId).subscribe({
      next: (data: any) => {
        this.user = data;
      },
      error: (error: any) => {
        console.log("Error on getting a single user: " + error);
      }
    });
  }


  
  getAllUsers() {
    this.userService.getAllUsers().subscribe({
      next: (data: any) => {
        this.userList = data;
        this.route.params.subscribe(params => {
          const userId = params['id'];
          const selectedUser = this.userList.find(u => u.id == userId)
    
          if (userId && selectedUser) {
            this.user.id = userId;
            this.selectedUser = selectedUser;
            this.getAllRepsOfUser(userId)
          }
        }); 
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    })
  }



getAllRepsOfUser(id: number) {
  return this.userService.getUserRep(id).subscribe({
      next: (data: any) => {
        this.repList = data;
      },
      error: (error: any) => {
        console.log("error on reps fetching" + error);
      }
    });
}

seeRep(user: User) {
  this.selectedUser = user;
  
  // Call getAllRepsOfUser and wait for it to complete
  this.getAllRepsOfUser(user.id)
    
}

toString(rep: any) {
   return JSON.stringify(rep)
  }


  selectUser(selectedUser: Event) {
    // Implement your logic when a day is selected
    console.log('Selected user:', selectedUser);
    const user = this.userList.find(u => u.name === selectedUser.toString());
    if (user){
      this.isUserSelected[user.id] = !this.isUserSelected[user.id]
    }

  }

  addRep(id: number){
    console.log("coucou")
    this.router.navigate(['/add-rep', id] );
  }

  redirectToModify(id: number){
    console.log("modify rep : "+ id)
    this.router.navigate(['/modifyRep', id]);
  }

  deleteRepById(id: number) {
    console.log("del" + id);
    const rep = this.repList.find(u => u.id === id);
    if (rep){
    this.userService.deleteRepById(id).subscribe(
      () => {
        console.log(`Deleted rep with ID: ${id} for user ${rep.userId}`);
        this.getAllRepsOfUser(rep.userId);
        // this.cdr.detectChanges(); // Trigger change detection
      },
      (error) => {
        console.error(`Error deleting rep with ID ${id}:`, error);
        // Handle any error that occurred during the delete request.
      }
    );
    }
  }
}
