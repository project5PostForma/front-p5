import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepertoiresComponent } from './repertoires.component';

describe('RepertoiresComponent', () => {
  let component: RepertoiresComponent;
  let fixture: ComponentFixture<RepertoiresComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RepertoiresComponent]
    });
    fixture = TestBed.createComponent(RepertoiresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
