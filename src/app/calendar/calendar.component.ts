import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import frLocale from '@fullcalendar/core/locales/fr'; // Import the French locale
import { User } from '../models/user.model';
import { UserService } from '../user.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements AfterViewInit {
  userList: User[] = []; // Define the userList property to hold a list of users
  hourAdded = 0;
  minAdded = 0;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    // Fetch userList data and then initialize the calendar
    this.userService.getAllUsers().subscribe({
      next: (data: any) => {
        this.userList = data;
        this.initializeCalendar(); // Call the calendar initialization method once data is available
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    });
  }

  initializeCalendar(): void {
    // Access the DOM element using ViewChild
    const calendarElement = document.getElementById('calendar');


  // Create an array to hold events for all users
  const allEvents: { title: string; start: string; end: string; }[] = [];

    this.userList.forEach((user) => {

      // Create an array to hold the events for this user
      const events = user.lessons.map((lesson) => {
        // Split the time into hours and minutes
        const [hours, minutes] = user.slot.split(':').map(Number);

        // Add 30 min, 45 min or 1h
        if (user.lessonDuration == '1h') {
          this.hourAdded = 1
          this.minAdded = 0
        }
        if (user.lessonDuration == '45 min') {
          this.hourAdded = 0
          this.minAdded = 45
        }
        if (user.lessonDuration == '30 min') {
          this.hourAdded = 0
          this.minAdded = 30
        }
        const newHours = hours + this.hourAdded;
        const newMins = minutes + this.minAdded;


        // Format the new time with leading zeros
        const endTime = `${newHours.toString().padStart(2, '0')}:${newMins.toString().padStart(2, '0')}`;

        // Define the event object
        const event = {

          title: user.name,
          start: `${lesson.date.toString().split('T')[0]}T${user.slot.split('h')[0]}:00`,
          end: `${lesson.date.toString().split('T')[0]}T${(endTime).split('h')[0]}:00`,
        };

        // Push the event into the array for all events
        allEvents.push(event);

        // You can place the console.log statement here to log each event
        console.log(user);

        return event; // Return the event object
      });

      // Check if the element exists before initializing FullCalendar
      if (calendarElement) {
        // Initialize FullCalendar with the required plugins
        const calendar = new Calendar(calendarElement, {
          plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
          initialView: 'dayGridMonth',
          slotMinTime: '08:00:00',
          slotMaxTime: '20:00:00',
          events: allEvents,
          locale: frLocale,
          views: {
            dayGridMonth: {
              dayMaxEventRows: 3,
              dayMaxEvents: true,
            },
          },
          dateClick: (info) => {
            // Handle date clicks in your custom year view here
            console.log('Clicked date:', info.dateStr);
            // Perform any actions based on the selected date
          },
        });

        // Render the calendar
        calendar.render();

        // Add navigation controls (e.g., buttons) to switch between views
        const weekViewButton = document.getElementById('week-view-button');
        const monthViewButton = document.getElementById('month-view-button');

        if (weekViewButton && monthViewButton) {
          weekViewButton.addEventListener('click', () => {
            calendar.changeView('timeGridWeek');
          });

          monthViewButton.addEventListener('click', () => {
            calendar.changeView('dayGridMonth');
          });
        }
      }
    });
  }

  ngAfterViewInit(): void {
    // Do nothing here; initialization is now handled in ngOnInit
  }
}
