import { Component, numberAttribute, OnInit } from '@angular/core';
import { Lesson } from '../models/lesson.model';
import { User } from '../models/user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {
  lessonPayedStatus: { [day: string]: { [timeSlot: string]: boolean } } = {};
  // isHovered: { [day: string]: { [timeSlot: string]: boolean } } = {};
  dateOfTodayLesson =  new Date();
  userList: User[] = [];
  user!: User;
  now = new Date();

  timeSlots: string[] = ['08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00'];
  daysOfWeek: string[] = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi','Samedi','Dimanche'];


  ngOnInit(): void {
    this.getAllUsers();
  }

  constructor(
    private userService: UserService,
  ) {      // Initialize lessonPayedStatus with default values 
    this.daysOfWeek.forEach(day => {
      this.lessonPayedStatus[day] = {};
      this.timeSlots.forEach(timeSlot => {
        // Initialize lessonPayedStatus from localStorage or with default values
        const storedLessonPayedStatus = localStorage.getItem(`lessonPayedStatus_${day}_${timeSlot}`);
        this.lessonPayedStatus[day][timeSlot] = storedLessonPayedStatus ? JSON.parse(storedLessonPayedStatus) : false;
            });
    });

  }

 getCurrentWeekNumber(): number {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const firstDayOfYear = new Date(currentYear, 0, 1); // January is 0
    const daysSinceFirstDay = Math.floor((currentDate.getTime() - firstDayOfYear.getTime()) / (24 * 60 * 60 * 1000));
    const currentWeekNumber = Math.ceil((daysSinceFirstDay + 1) / 7);
    return currentWeekNumber;
  }
  
  currentWeek = this.getCurrentWeekNumber();

  getLastMondayDayMonthYear(): { day: number, month: string, year: number } {
    const currentDate = new Date();
    const currentDayOfWeek = currentDate.getDay(); // Sunday is 0, Monday is 1, etc.
    const daysUntilLastMonday = currentDayOfWeek === 0 ? 6 : currentDayOfWeek - 1;
    const lastMondayDate = new Date(currentDate);
    lastMondayDate.setDate(currentDate.getDate() - daysUntilLastMonday);
  
    const lastMondayDay = lastMondayDate.getDate(); // Day of the month
    const lastMondayMonth = this.getMonthNameInFrench(lastMondayDate); // Month (0 for January, 1 for February, etc.)
    const lastMondayYear = lastMondayDate.getFullYear(); // Year
  
    return { day: lastMondayDay, month: lastMondayMonth, year: lastMondayYear };
  }
  
  getCurrentDayOfWeek(): number {
    return (new Date()).getDay();
  }



    
  getNextFridayDayMonthYear(): { day: number, month: string, year: number } {
    const currentDate = new Date();
    const currentDayOfWeek = currentDate.getDay(); // Sunday is 0, Monday is 1, etc.
    const daysUntilNextFriday = currentDayOfWeek === 6 ? 1 : 5 - currentDayOfWeek;
    const nextFridayDate = new Date(currentDate);
    nextFridayDate.setDate(currentDate.getDate() + daysUntilNextFriday);
  
    const nextFridayDay = nextFridayDate.getDate(); // Day of the month
    const nextFridayMonth = this.getMonthNameInFrench(nextFridayDate); // Month in French
    const nextFridayYear = nextFridayDate.getFullYear(); // Year
  
    return { day: nextFridayDay, month: nextFridayMonth, year: nextFridayYear };
  }

  getMonthNameInFrench(date: Date): string {
    const monthNames = [
      "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
      "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
    ];
  
    return monthNames[date.getMonth()];
  }

  lastMondayInfo = this.getLastMondayDayMonthYear();
  nextFridayInfo = this.getNextFridayDayMonthYear();


  getAllUsers() {
    this.userService.getAllUsers().subscribe({
      next: (data: any) => {
        this.userList = data;
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    })
  }

  setLessonPayment(userId: number, updatedLesson: Lesson){
    this.userService.setLessonPayment(userId, updatedLesson).subscribe({
      next: (data: any) => {
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    })
  }


  getLessonDateOfYear(day: string){
    const dateOfLesson = this.getLastMondayDayMonthYear() 
    const index = this.daysOfWeek.indexOf(day);
    this.dateOfTodayLesson.setDate(dateOfLesson.day + index);

    return this.dateOfTodayLesson;
  }


  // Function to retrieve user or slot information for a given day and time slot
  // retourne un couple user (pour l'affichage) - lessonId (pour validation paiement)
  // getUserForTimeSlot(day: string, timeSlot: string): String {
  //   const user = this.userList.find(u => u.slot === timeSlot && u.lessonDay === day);
  //   return user ? user.name : '';
  // }

  


    // Function to retrieve user or slot information for a given day and time slot
    getUserForTimeSlot(day: string, timeSlot: string): String {
      const user = this.userList.find(u => u.slot === timeSlot && u.lessonDay === day);
      return user ? user.name : '';
    }
  


  // Sample data for lessons (replace with actual data)
  lessons: { [day: string]: { [timeSlot: string]: boolean } } = {};



  // Function to toggle the payment status of a lesson
  togglePaymentStatus(day: string, timeSlot: string): Lesson {
    this.lessonPayedStatus[day][timeSlot] = !this.lessonPayedStatus[day][timeSlot]
    localStorage.setItem(`lessonPayedStatus_${day}_${timeSlot}`, JSON.stringify(this.lessonPayedStatus[day][timeSlot]));    const user = this.userList.find(u => u.slot === timeSlot && u.lessonDay === day);
    if (user) {
      // Toggle the payment status for the found user's lesson
      const lesson = user.lessons.find(lesson => {
        const lessonDate = lesson.date.toString().split('T')[0];
        const userLessonDate = this.getLessonDateOfYear(user.lessonDay).toISOString().split('T')[0];

        return lessonDate === userLessonDate;
      }); 
           if (lesson) {
            lesson.isPayed = !lesson.isPayed;
            console.log("toggle lesson ispayed"+lesson.isPayed)

            this.setLessonPayment(user.id, lesson)

        // Invert the payment status
        return lesson

      }

    }
    return new Lesson
  }



  setLessonPayedStatus(day: string, timeSlot: string) {
    this.lessonPayedStatus[day][timeSlot] = !this.lessonPayedStatus[day][timeSlot];
    localStorage.setItem(`lessonPayedStatus_${day}_${timeSlot}`, JSON.stringify(this.lessonPayedStatus[day][timeSlot]));    const user = this.userList.find(u => u.slot === timeSlot && u.lessonDay === day);

    if (user && user.lessons && user.lessons.length > 0) {
      const lesson = this.togglePaymentStatus(day, timeSlot);
      console.log("setLessonPayedStatus : lesson ");
      console.log(lesson);

      console.log("lesson.ispayed")
      console.log(lesson.isPayed);
      return lesson.isPayed; // Replace this with how you determine the lesson's payment status

    }
    return false;
  }



}
