import { LocaleSingularArg } from "fullcalendar";
import { User } from "./user.model";

export class Lesson {
    isPayed!: Boolean;
    date!: Date;
    day!: string;
    userId!: number;
  };

