export interface UserLessonPair {
  user: string; // Replace with the actual type of the user object
  lessonId: string; // Replace with the actual type of the lessonId
}