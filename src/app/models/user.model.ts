import { LocaleSingularArg } from "fullcalendar";
import { Lesson } from "./lesson.model";
import { Rep } from "./rep.model";

export class User {
    id!: number;
    name!: string;
    firstname!: String;
    price!: number;
    slot!: String;
    phone!: String;
    lessonDay!: string;
    lessonDuration!: String;
    lessons!: Lesson[];
    reps!: Rep[];
    paymentStatus!: number;
    givenLessons!: number;
}

