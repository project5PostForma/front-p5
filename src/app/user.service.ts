import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './models/user.model';
import { Lesson } from './models/lesson.model';
import { Observable } from 'rxjs';
import { Rep } from './models/rep.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl = environment.UsersDomain 

  constructor(private http: HttpClient) { }

  getAllLessons() {
    return this.http.get<any[]>(`${this.baseUrl}/allLessons`);
  }

  addLessonToUser(id: number, lesson: Lesson){
    return this.http.post<any[]>(`${this.baseUrl}/addLessonToUser/${id}`, lesson);
  }

  addRepToUser(id: number, rep: Rep){
    const url = `${this.baseUrl}/addRepToUser/${id}`;
    return this.http.post<Rep>(url, rep);
  }

  getAllUsers() {
    return this.http.get<any[]>(`${this.baseUrl}/all`);
  }


  getUser(id: number): Observable<User> {
    console.log(`${this.baseUrl}/getUser/${id}`)
    return this.http.get<User>(`${this.baseUrl}/getUser/${id}`);
  }

  getUserRep(id: number): Observable<Rep> {
    console.log(`${this.baseUrl}/getUserRep/${id}`)
    return this.http.get<Rep>(`${this.baseUrl}/getUserRep/${id}`);
  }

  getUserSingleRep(repId: number): Observable<Rep> {
    console.log(`${this.baseUrl}/getUserSingleRep/${repId}`)
    return this.http.get<Rep>(`${this.baseUrl}/getUserSingleRep/${repId}`);
  }

  addUser(user: User){
    const url = `${this.baseUrl}/addUser`;
    return this.http.post<User>(url, user);
  }



  addYearLessonsToUser(id: number, lessons: Lesson[]){
    return this.http.post<any[]>(`${this.baseUrl}/addYearLessonsToUser/${id}`, lessons);
  }

  deleteUserById(id: number){
    const url = `${this.baseUrl}/deleteUserById/${id}`;
    console.log(url)
    return this.http.delete(url);
  }

  deleteRepById(id: number){
    const url = `${this.baseUrl}/deleteRepById/${id}`;
    console.log(url)
    return this.http.delete(url);
  }

  modifyUserById(id: number, updateduser: User){
    const url = `${this.baseUrl}/modifyUser/${id}`;
    return this.http.put<User>(url, updateduser);
  }

  modifyRepById(id: number, updatedRep: Rep){
    const url = `${this.baseUrl}/modifyRep/${id}`;
    return this.http.put<Rep>(url, updatedRep);
  }
  
  setLessonPayment(userId: number, updatedLesson: Lesson){
    const url = `${this.baseUrl}/setLessonPayment/${userId}`;
    return this.http.put<User>(url, updatedLesson);
  }


}
