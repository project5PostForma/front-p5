import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { Lesson } from '../models/lesson.model';
import { User } from '../models/user.model';
import { PrintService } from '../print.service';

import { UserService } from '../user.service';

@Component({
  selector: 'app-compta',
  templateUrl: './compta.component.html',
  styleUrls: ['./compta.component.scss']
})



export class ComptaComponent implements OnInit {
  
  lessonList: Lesson[] = [];
  userList: User[] = []; // Define the userList property to hold a list of users
  total: number = 0;

  constructor(
    private userService: UserService,
    private router: Router,
    private printService: PrintService
  ){}


  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe({
      next: (data: any) => {
        this.userList = data;
        console.log(data);
        this.getTotal();
      },
      error: (error: any) => {
        console.log("error on users fetching" + error);
      }
    })
  }

  getTotal(){
    for (let user of this.userList){
      for (let lesson of user.lessons){
        if (lesson.isPayed == true){
          this.total += user.price
        }
      }
    }
    console.log("total " +  this.total)
  }

  redirectToModify(id: number){
    this.router.navigate(['/modifyUser', id]);
  }

  //  generatePdf() {
  //   if (this.pageContainer) {
  //     const element = this.pageContainer.nativeElement;
  //     const options = {
  //       margin: 10,
  //       filename: 'your-document.pdf',
  //       image: { type: 'jpeg', quality: 0.98 },
  //       html2canvas: { scale: 2 },
  //       jsPDF: { unit: 'mm', format: 'a4', orientation: 'portrait' },
  //     };

  //     html2pdf().from(element).set(options).outputPdf().then((pdf) => {
  //       // You can save or display the PDF here
  //       // Example: pdf.save('your-document.pdf');
  //     });
  //   } else {
  //     console.error('pageContainer is not defined.');
  //   }

  @ViewChild('pageContainer') pageContainer: ElementRef | undefined;


  generatePdf() {
    if (this.pageContainer) {
      const element = this.pageContainer.nativeElement;

          html2canvas(element).then((canvas) => {
        let fileWidth = 208;
        let fileHeight = (canvas.height * fileWidth) / canvas.width;
  
        const FILEURI = canvas.toDataURL('image/png');
        let PDF = new jsPDF('p', 'mm', 'a4');
        let position = 0;
        PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight);
  
        PDF.save('compta.pdf');
      });
    }
  }

  printDocument(): void {
    window.print(); // This will trigger the browser's print dialog
  }

}



  // generatePdf() {
  //   const documentDefinition = {
  //     content: [
  //       { text: 'Récapitulatif du mois', style: 'header' },
  //       {
  //         table: {
  //           headers: ['Date', 'Élève', 'Paiement'],
  //           body: this.lessonList.map((lesson) => [
  //             lesson.date,
  //             lesson.userId,
  //             lesson.isPayed
  //           ])
  //         }
  //       }
  //     ],
  //     styles: {
  //       header: {
  //         fontSize: 18,
  //         bold: true
  //       }
  //     }
  //   };

  //   const pdf = pdfMake.createPdf(documentDefinition);
  //   pdf.download('recapitulatif.pdf');
  // }

