import { Injectable } from '@angular/core';

@Injectable()
export class PrintService {
  constructor() {}

  printDocument(documentId: string): void {
    const documentToPrint = document.getElementById(documentId);
    if (documentToPrint) {
      const windowObject = window.open('', '_blank', 'width=600,height=600');
      if (windowObject) {
        windowObject.document.open();
        windowObject.document.write('<html><head><title>Print</title></head><body>');
        windowObject.document.write(documentToPrint.innerHTML);
        windowObject.document.write('</body></html>');
        windowObject.document.close();
        windowObject.print();
        windowObject.close();
      } else {
        console.error('Failed to open a new window for printing.');
      }
    } else {
      console.error(`Document with id '${documentId}' not found.`);
    }
  }
}
