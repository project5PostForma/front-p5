import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddRepComponent } from './add-rep/add-rep.component';
import { AdduserComponent } from './adduser/adduser.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ComptaComponent } from './compta/compta.component';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { ModifyRepComponent } from './modify-rep/modify-rep.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';
import { RepertoiresComponent } from './repertoires/repertoires.component';
import { StatsComponent } from './stats/stats.component';
import { ValidationComponent } from './validation/validation.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: 'home', component: HomeComponent},
  {path: 'list', component: ListComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: 'validation', component: ValidationComponent},
  {path: 'compta', component: ComptaComponent},
  {path: 'addUser', component: AdduserComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'modifyUser/:id', component: ModifyUserComponent},
  {path: 'rep/:id', component: RepertoiresComponent},
  {path: 'add-rep/:id', component: AddRepComponent},
  {path: 'modifyRep/:id', component: ModifyRepComponent},

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true, scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {}


