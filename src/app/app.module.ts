import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatMomentDateModule, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ListComponent } from './list/list.component';
import { CalendarComponent } from './calendar/calendar.component';
import { ComptaComponent } from './compta/compta.component';
import { AdduserComponent } from './adduser/adduser.component';
import { HttpClientModule } from '@angular/common/http';
import { StatsComponent } from './stats/stats.component';
import { ModifyUserComponent } from './modify-user/modify-user.component';
import { PopupComponent } from './popup/popup.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ValidationComponent } from './validation/validation.component';
import { RepertoiresComponent } from './repertoires/repertoires.component';
import { AddRepComponent } from './add-rep/add-rep.component';
import { ModifyRepComponent } from './modify-rep/modify-rep.component';
import { PrintService } from './print.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ListComponent,
    CalendarComponent,
    ValidationComponent,
    ComptaComponent,
    AdduserComponent,
    StatsComponent,
    ModifyUserComponent,
    PopupComponent,
    RepertoiresComponent,
    AddRepComponent,
    ModifyRepComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatMomentDateModule, // Use MatMomentDateModule for the Date-fns adapter
    MatSlideToggleModule
  ],
  providers: [
    PrintService,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],  bootstrap: [AppComponent]
})
export class AppModule { }
